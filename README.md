Instalacion 
=============
Requisitos previos: 
-------------------
- NodeJS v10.16
- npm 6.9.0
- ionic client generator 5.2.4 (para instalar npm -g install ionic)
- git 2.22

Despliegue:
------------

Clonar el proyecto  del repositorio de gitlab

> $ git clone https://gitlab.com/cromeocromeo/todos.git 

ingresar al proyecto 

> $ cd todos

instalar paquetes

> $ npm install 

Hacer correr de anera local 

> $ ionic serve

Abrir en el navegador la url 

> http://localhost:8100

que es la direccion por defecto.






