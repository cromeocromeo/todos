import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TodosService {
  url = 'http://jsonplaceholder.typicode.com'
  constructor(private http: HttpClient) { 

  }
  list(selectuser){
    if(selectuser.id == 0){
      return this.http.get(this.url +'/posts');
    }
    return this.http.get(this.url +'/posts?userId=' + selectuser.id);
  }
  view(id){
    return this.http.get(this.url + '/posts/' + id);
  }
  comments(id){
    return this.http.get(this.url + '/posts/' + id + '/comments');
  }
  edit(todo:any){
    return this.http.put(this.url + '/posts/' + todo.id, todo );
  }
  delete(todo:any){
    return this.http.delete(this.url + '/posts/' + todo.id);
  }
  users(){
    return this.http.get(this.url + '/users');
  }
}
