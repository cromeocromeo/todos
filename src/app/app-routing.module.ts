import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'list', loadChildren: './pages/todos/list/list.module#ListPageModule' },
  { path: 'add', loadChildren: './pages/todos/add/add.module#AddPageModule' },
  { path: 'view/:id', loadChildren: './pages/todos/view/view.module#ViewPageModule' },
  { path: 'edit/:id', loadChildren: './pages/todos/edit/edit.module#EditPageModule' },
  { path: 'delete/:id', loadChildren: './pages/todos/delete/delete.module#DeletePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
