import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  id:any = null;
  todo:any = {};
  titleAux:any = '';
  constructor( private todService: TodosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public alertController: AlertController
  ) { 
    
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Todo update',
      subHeader: '',
      message: '',
      buttons: [
         {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/list']);
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.todService.view(this.id).subscribe(result => {
      this.todo = result;
      this.titleAux = this.todo.title;
    });
  }
  save() {
    this.todService.edit(this.todo).subscribe(result => {
      this.todo = result;
      this.presentAlert();
    });
  }
}
