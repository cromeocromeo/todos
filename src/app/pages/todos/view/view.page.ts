import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.page.html',
  styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
  todo:any = {};
  coments:any = [];
  id:any = 0;
  constructor(
    private todService: TodosService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');;
    this.todService.view(this.id).subscribe(result => {
      this.todo = result;
    });
    this.todService.comments(this.  id).subscribe(result => {
      this.coments = result;
    });
  }

}
