import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/services/todos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.page.html',
  styleUrls: ['./delete.page.scss'],
})
export class DeletePage implements OnInit {
  todo:any = {};
  coments:any = [];
  id:any = 0;
  constructor(
    private todService: TodosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public alertController: AlertController
  ) { }


  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');;
    this.todService.view(this.id).subscribe(result => {
      this.todo = result;
    });
    this.todService.comments(this.  id).subscribe(result => {
      this.coments = result;
    });
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Todo Delete',
      subHeader: '',
      message: '',
      buttons: [
         {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/list']);
          }
        }
      ]
    });
    await alert.present();
  }
  delete(){
    this.todService.delete(this.todo).subscribe(result => {
      this.todo = result;
      this.presentAlert();
    });
  }

}
