import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TodosService } from '../../../services/todos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  results:any=[];
  todo: any = {};
  users: any = [];
  selectUser:any = {};
  userAll = {id:0, name: 'All users'};
 constructor(private todService: TodosService, private route: ActivatedRoute) { 
    this.selectUser = this.userAll;
  }

  ngOnInit() {
    this.todService.users().subscribe(users => {
      this.users = users;
      this.users.unshift(this.userAll);      
    });
    this.todService.list(this.selectUser).subscribe(result => {
      this.results = result;
      this.results.map(key => (key.id) == (this.todo.id) ? this.todo : key)
    });
  }

  buscar() {
    this.todService.list(this.selectUser).subscribe(result => {
      this.results = result;
      this.results.map(key => (key.id) == (this.todo.id) ? this.todo : key)
    });
  }
}
